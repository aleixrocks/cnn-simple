#!/bin/bash

PARSE=./parse_script.sh

# sudo mount -o remount,mode=755 /sys/kernel/debug
# echo -1  | sudo tee /proc/sys/kernel/perf_event_paranoid

sudo bash -c "cpupower frequency-set -g performance" > /dev/null


#LTTNGW=lttng-libc.sh
#TRACE="lttng-kernel.sh -a bm-pt-1024s-128b-jemalloc $LTTNGW"
#TRACE="lttng-kernel.sh -m bm-pt-oversubs $LTTNGW"
#TRACE="lttng-kernel.sh -t bm-pt-malloc $LTTNGW"
TRACE="perf record -F999 -g"
#TRACE="perf record -e 'kmem:mm_*' -a"
#TRACE="perf record -e syscalls:sys_enter_sched_yield"
#TRACE=ftrace_function_tracer.sh
#TRACE=lockdep_stat.sh

TASKSET="numactl --membind=0 --cpunodebind=0 numactl --physcpubind=+0-39"
#TASKSET="numactl --membind=0 --cpunodebind=0 numactl --physcpubind=+0-7"

# Intel MKL-DNN optimization stuff
export KMP_BLOCKTIME=0 #recommended for non-CNN (0 or 1). value in ms.
export KMP_AFFINITY=granularity=fine,verbose,compact,1,0

# Python stuff
#export PYTHONMALLOCSTATS="enable"
#export PYTHONMALLOC=malloc
#export PYTHONTRACEMALLOC=1

# Glibc stuff
#export M_MMAP_THRESHOLD=$((4**1024*1024*8))
#export M_TRIM_THRESHOLD=-1

# jemalloc stuff
#export LD_PRELOAD=$HOME/tools/jemalloc/install/lib/libjemalloc.so:$LD_PRELOAD
#export MALLOC_CONF="confirm_conf:true,abort_conf:true"
#export MALLOC_CONF=${MALLOC_CONF}",dirty_decay_ms:-1,muzzy_decay_ms:-1"
#export MALLOC_CONF="stats_print:true" # might deadlock
#export MALLOC_CONF="prof:true,lg_prof_sample:1,prof_accum:false,prof_prefix:jeprof.out"

SIZE=1024
BATCH=128
CMD="./igs.py"
LTTNG_UST_PYTHON_REGISTER_TIMEOUT=-1 LTTNG_UST_REGISTER_TIMEOUT=-1 $TASKSET $TRACE $CMD

#SIZE=1024
#BATCH=128
#NAME="bm-pt-${SIZE}s-${BATCH}b-1skt"
#CMD="./igs.py"
#grapher.sh -v -o ${NAME}-scalability-test -p $PARSE -y nw -s "$CMD:5:40 32 20 16 8 4 2 1:bm-pt-big"


sudo bash -c "cpupower frequency-set -g conservative" > /dev/null
