#!/usr/bin/env python3

# -*- coding: utf-8 -*-
import torch
import torch.nn as nn

import torchvision.models as models
import numpy as np


class MyNet(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(MyNet, self).__init__()
        hid_channels=32
        w = 224
        h = 224
        self.lc = w*h*hid_channels

        # apply a convolutional kernel of size 3x3, advancing by 1 pixel, and
        # with zero padding of 1 pixel. The kernel applies at the three RGB layers and
        # outputs "hid_channels" layers. This number of layers are useful for the
        # NN to extract features. Because we use stride 1, the image itself are not
        # reduced. We also use padding 1 to avoid reducing the image size.

        # The key point here is to match the output size of one layer with the
        # input size of the next layer.
        self.conv = nn.Conv2d(in_channels=in_channels, kernel_size=3, out_channels=hid_channels, stride=1, padding=1)
        self.fc = nn.Linear(in_features=self.lc, out_features=out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = nn.functional.relu(x)
        # At this point we still have an image. Now we are done with the
        # initial CNN image processing and want to pipe the date into the neural network.
        # To do so, we transform the matrix (image) into a vector. We need to specify the
        # vector size which is high*width*channels. Our previous convolution has created
        # "hid_channels".
        x = x.view(-1, self.lc)
        # the linear regression is the trainable element here and it converts all the
        # "unfolded" hidden inputs into the specified number of outputs.
        x = self.fc(x)

        return x


def get_cubes(dims=3, edge=8, channels=1, cnt_samples=16, cnt_classes=2, channels_first=True, onehot=False):
   if channels_first:
       shape = tuple([cnt_samples, channels] + [edge] * dims)
   else:
       shape = tuple([cnt_samples] + [edge] * dims + [channels])
   X = np.zeros(shape, dtype=np.float32)
   Y = np.zeros(cnt_samples, dtype=np.int32)
   for i in range(cnt_samples):
       if i % 2 == 1:
           X[i, :] = np.ones(shape[1:])
           Y[i] = 1
   return X, Y


# Input parameters
size = 1024
batch = 128

### Generate data the raw way
N, D_in, D_out, H, W = size, 3, 1000, 224, 224
x_train = torch.randn(N, D_in, H, W, dtype=torch.float32)
y_train = torch.zeros(N, dtype=torch.long)
### Generate data using benchmaker function (the raw way II)
#x_train, y_train = get_cubes(dims=2, edge=224, channels=3, cnt_samples=size, channels_first=True, onehot=False)
#x_train = torch.from_numpy(x_train)
#y_train = torch.from_numpy(y_train.astype(np.int64))


### models
#model = MyNet(D_in, D_out)
##model = models.resnet152()
model = models.resnet50()
##model = models.resnet18()


train_dataset = torch.utils.data.TensorDataset(x_train, y_train)
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch, shuffle=True)

# The nn package also contains definitions of popular loss functions; 
#loss_fn = torch.nn.MSELoss(reduction='sum')
loss_fn = torch.nn.CrossEntropyLoss()

learning_rate = 1e-4
#for t in range(10):
for t, (x, y) in enumerate(train_loader):
    # Forward pass: compute predicted y by passing x to the model. Module objects override the __call__ operator so you can call them like functions. When doing so you pass a Tensor of input data to the Module and it producesa Tensor of output data.
    y_pred = model(x)

    # print tensors
    #print(y_pred.size())
    #print(y.size())
    #print(y_pred)
    #print(y)

    loss = loss_fn(y_pred, y)
    print(t, loss.item())

    # Zero the gradients before running the backward pass.
    model.zero_grad()

    # Backward pass: compute gradient of the loss with respect to all the learnable parameters of the model. Internally, the parameters of each Module are stored in Tensors with requires_grad=True, so this call will compute gradients for all learnable parameters in the model.
    loss.backward()

    # Update the weights using gradient descent. Each parameter is a Tensor, so we can access its gradients like we did before.
    with torch.no_grad():
        for param in model.parameters():
            param -= learning_rate * param.grad
